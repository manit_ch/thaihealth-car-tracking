angular.module('starter', ['ionic','ionic.service.core', 'starter.controllers', 'starter.directives', 'starter.services'])

/*.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    if(window.StatusBar) {
      StatusBar.styleDefault();
    }
  });
})
*/

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleLightContent();
    }
  });
})
.config(function($stateProvider, $urlRouterProvider) {

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider

  // setup an abstract state for the tabs directive
    .state('tab', {
    url: '/tab',
    abstract: true,
    templateUrl: 'templates/tabsController.html'
  })

  // Each tab has its own nav history stack:

  .state('tab.map', {
    url: '/map',
    views: {
      'tab-map': {
        templateUrl: 'templates/map.html',
        controller: 'MapCtrl'
      }
    }
  })

  .state('tab.details', {
      url: '/details',
      views: {
        'tab-details': {
          templateUrl: 'templates/details.html',
          controller: 'detailsCtrl'
        }
      }
    })

      .state('tab.booking_route', {
          url: '/booking_route',
          views: {
              'tab-booking': {
                  templateUrl: 'templates/booking_route.html',
                  controller: 'bookingCtrl'
              }
          }
      })

      .state('tab.booking_time', {
          url: '/booking_time/{route_id}',
          views: {
              'tab-booking': {
                  templateUrl: 'templates/booking_time.html',
                  controller: 'bookingCtrl'
              }
          }
      })


  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/tab/map');

});




// Add the 'ionic.service.core' module to your main angular module:
angular.module('test', ['ionic.service.core'])
// Identify App
.config(['$ionicAppProvider', function($ionicAppProvider) {
  // Identify app
  $ionicAppProvider.identify({
    // The App ID for the server
    app_id: 'd20ac530',
    // The API key all services will use for this app
    api_key: '5d0782b414f03028fc7c37c319d4ff29a6504959eb2060cf'
  });
}])

