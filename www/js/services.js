angular.module('starter.services', [])

.service('getService',
    function($http, $q){

        this.getProperty = function(param){
            var delay = $q.defer();
            var response = [];
            var params = param;
//            var SURL = 'http://www.jaotee.com/libraries/get_data.php?callback=JSON_CALLBACK&name=Gunner9';
            var SURL = 'https://il-solution.com/tracking/get_data2.php?callback=JSON_CALLBACK';  // JSONP production
//            var SURL = 'http://il-solution.com/tracking/php/dbprocess.php?callback=JSON_CALLBACK';  // JSONP
//            var SURL = 'http://localhost/tracking/php/dbprocess.php';
//            var SURL = 'http://localhost/thaihealthcartracking-bit/get_data.php?callback=JSON_CALLBACK';  // UAT
//            console.log("getService ser ");
            $http({
                method:'JSONP',
//                headers: {
//                    'Content-Type':'application/x-www-form-urlencoded;',
//                    'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*,q=0.8'
//                },
//                dataType: 'json',
                url: SURL
//                data: params
            }).success(function(data, status){
                if(angular.isObject(data)){
                    response = data;
//                    console.log("data is obj"+response.no1.tracking_datetime);
                    return delay.resolve(response);

                }else{
//                    var json_data_object = eval(data);
                    response = JSON.parse(data);
                    if(angular.isObject(response)){
                        console.log("response is obj"+response["no4"]);
                    }else{
                        response = JSON.parse(response);
//                        console.log("response is str"+ response["no4"]["tracking_longitude"]);
                    }
                    return delay.resolve(response);
                }

//                return delay.resolve(response);
            }).error(function(data){
                //alert("data erro ="+data);
                response = '{"message":"' + data + '","messageType":"error"  }';
                response = JSON.parse(response);
//                console.log("getService error "+data);
                return delay.reject(response);
            });

            return delay.promise;
        }
})

.service('getBookingService',
    function($http, $q){

        this.getProperty = function(serviceURL, serviceParams){
            var delay = $q.defer();
            var response = [];
//            var params = param;
            var paramData = JSON.stringify(serviceParams);
            var params = 'serviceMethod=' + serviceURL + '&serviceParams=' + JSON.stringify(serviceParams) + '';

//            var SURL = 'http://localhost/thaihealth-car-tracking/services/pGateway.php?'+params+'&callback=JSON_CALLBACK';  // JSONP UAT localhost
            var SURL = 'http://www.il-solution.com/tracking/services/pGateway.php?'+params+'&callback=JSON_CALLBACK';  // JSONP Production

            $http({
                method:'JSONP',
//                headers: {
//                    'Content-Type':'application/x-www-form-urlencoded;',
//                    'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*,q=0.8'
//                },
//                dataType: 'json',
                url: SURL,
                data: params
            }).success(function(data, status){
                if(angular.isObject(data)){
//                    console.log("response is obj1"+data);
                    response = data;
                    return delay.resolve(response);

                }else{
//                    var json_data_object = eval(data);
                    response = JSON.parse(data);
                    if(angular.isObject(response)){
//                        console.log("response is obj"+response["no4"]);
                    }else{
                        response = JSON.parse(response);
//                        console.log("response is str"+ response["no4"]["tracking_longitude"]);
                    }
                    return delay.resolve(response);
                }

//                return delay.resolve(response);
            }).error(function(data){
                response = '{"message":"' + data + '","messageType":"error"  }';
                response = JSON.parse(response);
//                console.log("getService error "+data);
                return delay.reject(response);
            });

            return delay.promise;
        }

    });

