angular.module('starter.controllers', [])

.controller('MapCtrl', function($scope, $ionicLoading, getService, $timeout, $rootScope) {

        $rootScope.tab = "map";
  $scope.mapCreated = function(map) {

//      var markers = [];
    function setMarkers(map, markers) {
        $rootScope.tab = "map";
       var imgArray = new Array();
        imgArray[0] = new Image();
        imgArray[0].src = 'img/icon_golft.png';
        imgArray[1] = new Image();
        imgArray[1].src = 'img/icon_vann.png';


       var response = getService.getProperty("test");
       response.then(function(value){

           if(value != '0' && value != 'error'){
               $scope.cars = value;


               function setMapOnAll(map) {
//                   markers.setMap(map);
                   for (var i = 0; i < markers.length; i++) {
                       markers[i].setMap(null);
                   }
               }
               function clearMarkers() {
                   setMapOnAll(null);
               }
               function deleteMarkers() {
                   clearMarkers();
                   markers = [];
               }
                if(angular.isDefined(markers) && markers.length > 0){
                    deleteMarkers();
                }
//               deleteMarkers();
//               alert("markers = "+markers);
//               alert($scope.cars["no4"][0]);
               for (var i = 1; i <= 5; i++) {
                   var car = value["no"+i];
//                   alert("car = "+car.tracking_latitude);

                  if (car.car_id <= '3') {
                   var myLatLng = new google.maps.LatLng(car.tracking_latitude, car.tracking_longitude);
                    var marker = new google.maps.Marker({
                       position: myLatLng,
                       map: map,
                       icon:imgArray[0].src,
                       title: car[0]
                   });
                      markers.push(marker);

                  }
                   else
                   {
                    var myLatLng = new google.maps.LatLng(car.tracking_latitude, car.tracking_longitude);
                     var marker = new google.maps.Marker({
                       position: myLatLng,
                       map: map,
                       icon:imgArray[1].src,
                       title: car[0]
                   });
                       markers.push(marker);
                   }
               }
           }
       }, function(error){
           $scope.cars = '';
//           alert(error);
       });

        var watch;
        watch = $timeout(function(){
//            console.log($rootScope.tab);
            if($rootScope.tab == "map"){
                setMarkers(map,markers);
            }else{
                $timeout.cancel(watch);
            }
        },5000);

   }
var markers = [];
    setMarkers(map, markers);
    $scope.map = map;

  };
})

.controller('detailsCtrl', function($scope, $ionicLoading, getService, $timeout, $rootScope) {
        $rootScope.tab = "carDetail";
        $scope.header = "รายระเอียดรถ";
        $scope.cars = [];
//        $scope.test = 0;
    //    {"0":"138811","1":"0","2":"0","3":"0","4":"2015-07-01 18:45:34","5":"1","6":"รถ Golf คันที่ 1","7":"รถ Golf","8":"1","9":"สายสีเขียว","tracking_id":"138811","tracking_latitude":"0","tracking_longitude":"0","tracking_speed":"0","tracking_datetime":"2015-07-01 18:45:34","car_id":"1","car_name":"รถ Golf คันที่ 1","car_type":"รถ Golf","routeline_id":"1","routeline_type":"สายสีเขียว"}
        function setDetails() {
            $rootScope.tab = "carDetail";
//            $scope.cars = [];
        var response = getService.getProperty("test");
        response.then(function(value){

            if(value != '0' && value != 'error'){
//                $scope.cars = [];
                $scope.cars = value;
//                $scope.test++;

//                console.log("data value ="+value.no1.tracking_datetime);
            }
        }, function(error){
            $scope.cars = [];
//            alert(error);
        });

            var watch;
            watch = $timeout(function(){
//                $scope.cars = [];
//                console.log($rootScope.tab);
                if($rootScope.tab == "carDetail"){
                    setDetails();
                }else{
                    $timeout.cancel(watch);
                }
            },5000);
        }

        setDetails();
})

.controller('bookingCtrl', function($scope, $ionicLoading, getBookingService, $timeout, $rootScope, $stateParams, $location) {
        $rootScope.tab = "booking";
        $scope.header = "เลือก รอบรถ";
        $scope.route = [];
        if(angular.isUndefined($rootScope.session)){
            $rootScope.session = [];
        }
        $scope.action_step = ""; // default step
        //    {"0":"138811","1":"0","2":"0","3":"0","4":"2015-07-01 18:45:34","5":"1","6":"รถ Golf คันที่ 1","7":"รถ Golf","8":"1","9":"สายสีเขียว","tracking_id":"138811","tracking_latitude":"0","tracking_longitude":"0","tracking_speed":"0","tracking_datetime":"2015-07-01 18:45:34","car_id":"1","car_name":"รถ Golf คันที่ 1","car_type":"รถ Golf","routeline_id":"1","routeline_type":"สายสีเขียว"}
        $scope.takeStep1 = function(){
            $scope.action_step = 1;
//            console.log("booking called");
            $scope.routes = [{"id":"1","destination":"ขาเข้า"},{"id":"2","destination":"ขาออก"}];
         /*   var serviceURL = "getRoutes";
            var serviceParams = [{value:999}];
            var response = getBookingService.getProperty(serviceURL, serviceParams);
            response.then(function(value){
                $scope.routes = value;
                console.log("data value ="+value);

            }, function(error){
                console.log("service error = "+error);
                $scope.msgShow = true;
            });
*/
        /*    var watch;
            watch = $timeout(function(){
//                $scope.cars = [];
                setDetails();
            },5000);
            */
        }

        $scope.takeStep2 = function(id){
            $scope.header = "เลือก รอบรถ";
            $scope.action_step = "chooseTime";
            $scope.route_select = "0";

            var serviceURL = "getRunningTime";
            var param = 0;
            if(id == 1){
                param = "3,4";
            }else if(id == 2){
                param = "1,2";
            }
            var serviceParams = [{route:param}];
            var response = getBookingService.getProperty(serviceURL, serviceParams);
            response.then(function(value){
                $scope.runningTime = value;
//                console.log("data value2 ="+value[0][0].time);

            }, function(error){
//                console.log("service error = "+error);
                $scope.msgShow = true;
            });
        };

        $scope.filterTime = function (param) {
            return function (item) {

//                console.log(item[0].time);

                var d = new Date();
                var hours = d.getHours();
                var minutes = d.getMinutes();
                if (hours   < 10) {hours   = "0"+hours;}
                if (minutes < 10) {minutes = "0"+minutes;}
                $scope.curTime = hours+":"+minutes+":00";

                if (item[0].time > $scope.curTime && (10-item[0].sum) > 0)
                {
                    return true;
                }
                return false;
            };
        };
        $scope.routeChange = function(value){
            $scope.route_select = value;
            var d = new Date();
            var hours = d.getHours();
            var minutes = d.getMinutes();
            if (hours   < 10) {hours   = "0"+hours;}
            if (minutes < 10) {minutes = "0"+minutes;}
            $scope.curTime = hours+":"+minutes+":00";
        };
        $scope.timeChange = function(value){
            console.log(value);
            $rootScope.seats = 1;
            var input = value.split(",");
            $scope.seatsAvaliable = input[5];
            $scope.Range = function(start, end) {
                var result = [];
                for (var i = start; i <= end; i++) {
                    result.push(i);
                }
                return result;
            };
        };

//        $scope.takeStep3 = function(running_time_id, route_id, time, origin, destination){
        $scope.takeStep3 = function(time, seats){
            if($scope.route_select == 0){
                alert("กรุณาเลือกเส้นทาง");
            }else if(angular.isUndefined(time) || time == null){
                alert("กรุณาเลือกเวลาเดินรถ");
            }else if(angular.isUndefined(seats) || seats == null || seats <= 0){
                alert("กรุณาเลือกจำนวนที่นั่ง");
            }else{
                var input = time.split(",");
                if(input[5] <= 0){
                    alert("รอบเวลาเดินรถในเส้นทางที่คุณเลือก เต็มแล้ว");
                }else if (confirm('Would you like to confirm?')) {

                    $rootScope.seats = seats;
                    $rootScope.time = input[2];
                    $rootScope.origin = input[3];
                    $rootScope.destination = input[4];
                    $scope.takeStep4(input[1], input[0], seats);
                }
            }
        };

        $scope.takeStep4 = function(running_time_id, route_id, seats){
            $scope.header = "ยืนยันการจอง";
            $scope.action_step = "confirm";
            var serviceURL = "confirmBooking";
            var serviceParams = [{route_id:route_id},{running_time_id:running_time_id},{seats:seats}];
            var response = getBookingService.getProperty(serviceURL, serviceParams);
            response.then(function(value){

                $scope.bookingData = value;
                if($scope.bookingData.status == 'confirmed'){
                    function dateFormat(){
                        var months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
                        var d = new Date();
                        var hours = d.getHours();
                        var minutes = d.getMinutes();
                        if (hours   < 10) {hours   = "0"+hours;}
                        if (minutes < 10) {minutes = "0"+minutes;}
                        var date = d.getDate()+" "+ months[d.getMonth()]+" "+d.getFullYear()+" / "+ hours+":"+ minutes+"  ";
                        return date;
                    }
                        $rootScope.session.push({
                            route_id:value.route_id,
                            running_time_id:value.running_time_id,
                            seats:seats,
                            origin:$rootScope.origin,
                            destination:$rootScope.destination,
                            time:$rootScope.time,
                            token:value.token,
                            confirmedDate:dateFormat()
                        });
                    $rootScope.seats = null;
                    $rootScope.time = null;
                    $rootScope.origin = null;
                    $rootScope.destination = null;

                }
//                $scope.ticket = 'confirmed';
//                console.log("data value ="+value);

            }, function(error){
//                console.log("service error = "+error);
                $scope.msgShow = true;
            });
        };

        if(angular.isUndefined($stateParams.route_id)){
            $location.path('/tab/booking_route');
        }else{
            if($stateParams.route_id == 1){
                $scope.road = "in";
                $scope.takeStep2($stateParams.route_id);
            }else if($stateParams.route_id == 2){
                $scope.road = "out";
                $scope.takeStep2($stateParams.route_id);
            }else if($stateParams.route_id == 3){

                $scope.header = "รายละเอียดการจองรถ";
                $scope.action_step = "show-reserved-data";
                if($rootScope.session.length >= 1){
                    $scope.reservedData = $rootScope.session;
                }else{
                    $scope.reservedData = 0;
                }
            }

        }
//        $scope.takeStep1(); // run first step
    });



