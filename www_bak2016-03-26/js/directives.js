angular.module('starter.directives', [])

.directive('map', function() {
  return {
    restrict: 'E',
    scope: {
      onCreate: '&'
    },
    link: function ($scope, $element, $attr) {

      var thaiHealth = new google.maps.LatLng(13.719471, 100.543287);
      var ngamDuphli = new google.maps.LatLng(13.724414, 100.548423);
      var centerMap = new google.maps.LatLng(13.718367, 100.542254);
      var iconThaiHealth = 'img/icon_office.png';
      function initialize() {
        var mapOptions = {
             center: centerMap,
             zoom: 15,
             mapTypeId: google.maps.MapTypeId.ROADMAP
            };
        var map = new google.maps.Map($element[0], mapOptions);
        $scope.onCreate({map: map});

        /***** ThaiHealth Marker*****/

        var markerThaiHealth = new google.maps.Marker({
          map:map,
          draggable:false,
          animation: google.maps.Animation.DROP,
          position: thaiHealth,
          icon:iconThaiHealth
            });

        /***** End of ThaiHealth Marker*****/

        /***** Green Line *****/

        var pointGreen1 = new google.maps.LatLng(13.724109, 100.549374);
	    var pointGreen2 = new google.maps.LatLng(13.720619, 100.547589);
	    var pointGreen3 = new google.maps.LatLng(13.721664, 100.546329);


	    var wps = [{location: pointGreen3}, {location: pointGreen2}, { location: pointGreen1 },  { location: ngamDuphli } ];
        var polylineOptionsActual = {
            strokeColor: '#33cd5f',
            strokeOpacity: 0.8,
            strokeWeight: 5
           };
	    directionsDisplay = new google.maps.DirectionsRenderer({ map: map, polylineOptions: polylineOptionsActual, suppressMarkers: true});
        var request = {
			origin: thaiHealth ,
			destination: thaiHealth,
			waypoints: wps,
			travelMode: google.maps.DirectionsTravelMode.DRIVING
			};

        directionsService = new google.maps.DirectionsService();
	    directionsService.route(request, function(response, status) {
				if (status == google.maps.DirectionsStatus.OK) {
					directionsDisplay.setDirections(response);
				}
				else
					alert ('failed to get directions');
             });

        /***** End Of Green Line *****/

        /***** Orange Line *****/


        var pointOrange1 = new google.maps.LatLng(13.719893, 100.543067);
	    var pointOrange2 = new google.maps.LatLng(13.717477, 100.541681);
	    var pointOrange3 = new google.maps.LatLng(13.714236, 100.535235);
        var pointOrange4 = new google.maps.LatLng(13.716625, 100.534038);
        var pointOrange5 = new google.maps.LatLng(13.716301, 100.533279);
        var pointOrange6 = new google.maps.LatLng(13.713862, 100.534444);

	    var wpsOrange = [{ location: pointOrange1 }, {location: pointOrange2}, {location: pointOrange3}, {location: pointOrange4}, {location: pointOrange5} ,{location: pointOrange6}];
        var polylineOptionsActualOrange = {
            strokeColor: '#ef473a',
            strokeOpacity: 0.8,
            strokeWeight: 5
           };
	    directionsDisplayOrange = new google.maps.DirectionsRenderer({ map: map, polylineOptions: polylineOptionsActualOrange, suppressMarkers: true  });
        var request1 = {
			origin: thaiHealth ,
			destination: thaiHealth,
			waypoints: wpsOrange,
			travelMode: google.maps.DirectionsTravelMode.DRIVING
			};

        directionsService = new google.maps.DirectionsService();
	    directionsService.route(request1, function(response, status) {
				if (status == google.maps.DirectionsStatus.OK) {
					directionsDisplayOrange.setDirections(response);
				}
				else
					alert ('failed to get directions');
             });

        /***** End of Orange Line *****/

        // Stop the side bar from dragging when mousedown/tapdown on the map
        google.maps.event.addDomListener($element[0], 'mousedown', function (e) {
          e.preventDefault();
          return false;
        });
      }




      function toggleBounce() {

        if (markerThaiHealth.getAnimation() != null) {
        markerThaiHealth.setAnimation(null);
        } else {
        markerThaiHealth.setAnimation(google.maps.Animation.BOUNCE);
        }
       }

      if (document.readyState === "complete") {
        initialize();
      } else {
        google.maps.event.addDomListener(window, 'load', initialize);
      }
    }
  }
});
