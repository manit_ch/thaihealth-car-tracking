angular.module('starter.controllers', ['webStorageModule'])

.controller('MapCtrl', function($scope, $ionicLoading, getService, $timeout) {


  $scope.mapCreated = function(map) {

//      var markers = [];
    function setMarkers(map, markers) {

       var imgArray = new Array();
        imgArray[0] = new Image();
        imgArray[0].src = 'img/icon_golft.png';
        imgArray[1] = new Image();
        imgArray[1].src = 'img/icon_vann.png';


       var response = getService.getProperty("test");
       response.then(function(value){

           if(value != '0' && value != 'error'){
               $scope.cars = value;


               function setMapOnAll(map) {
//                   markers.setMap(map);
                   for (var i = 0; i < markers.length; i++) {
                       markers[i].setMap(null);
                   }
               }
               function clearMarkers() {
                   setMapOnAll(null);
               }
               function deleteMarkers() {
                   clearMarkers();
                   markers = [];
               }
                if(angular.isDefined(markers) && markers.length > 0){
                    deleteMarkers();
                }
//               deleteMarkers();
//               alert("markers = "+markers);
//               alert($scope.cars["no4"][0]);
               for (var i = 1; i <= 5; i++) {
                   var car = value["no"+i];
//                   alert("car = "+car.tracking_latitude);

                  if (car.car_id <= '3') {
                   var myLatLng = new google.maps.LatLng(car.tracking_latitude, car.tracking_longitude);
                    var marker = new google.maps.Marker({
                       position: myLatLng,
                       map: map,
                       icon:imgArray[0].src,
                       title: car[0]
                   });
                      markers.push(marker);

                  }
                   else
                   {
                    var myLatLng = new google.maps.LatLng(car.tracking_latitude, car.tracking_longitude);
                     var marker = new google.maps.Marker({
                       position: myLatLng,
                       map: map,
                       icon:imgArray[1].src,
                       title: car[0]
                   });
                       markers.push(marker);
                   }
               }
           }
       }, function(error){
           $scope.cars = '';
//           alert(error);
       });

        var watch;
        watch = $timeout(function(){
            setMarkers(map,markers);
        },5000);

   }
var markers = [];
    setMarkers(map, markers);
    $scope.map = map;

  };
})

.controller('detailsCtrl', function($scope, $ionicLoading, getService, webStorage, $timeout, $rootScope) {
        //werer
$scope.header = "รายระเอียดรถ";
        $scope.cars = [];
//        $scope.test = 0;
    //    {"0":"138811","1":"0","2":"0","3":"0","4":"2015-07-01 18:45:34","5":"1","6":"รถ Golf คันที่ 1","7":"รถ Golf","8":"1","9":"สายสีเขียว","tracking_id":"138811","tracking_latitude":"0","tracking_longitude":"0","tracking_speed":"0","tracking_datetime":"2015-07-01 18:45:34","car_id":"1","car_name":"รถ Golf คันที่ 1","car_type":"รถ Golf","routeline_id":"1","routeline_type":"สายสีเขียว"}
        function setDetails() {

//            $scope.cars = [];
        var response = getService.getProperty("test");
        response.then(function(value){

            if(value != '0' && value != 'error'){
//                $scope.cars = [];
                $scope.cars = value;
//                $scope.test++;

                console.log("data value ="+value.no1.tracking_datetime);
            }
        }, function(error){
            $scope.cars = [];
//            alert(error);
        });

            var watch;
            watch = $timeout(function(){
//                $scope.cars = [];
                setDetails();
            },5000);
        }

        setDetails();
})

.controller('bookingCtrl', function($scope, $ionicLoading, getBookingService, webStorage, $timeout, $rootScope) {
        //werer
        $scope.header = "จองตั๋วรถ";
        $scope.route = [];
        $scope.action_step = 2; // default step
        //    {"0":"138811","1":"0","2":"0","3":"0","4":"2015-07-01 18:45:34","5":"1","6":"รถ Golf คันที่ 1","7":"รถ Golf","8":"1","9":"สายสีเขียว","tracking_id":"138811","tracking_latitude":"0","tracking_longitude":"0","tracking_speed":"0","tracking_datetime":"2015-07-01 18:45:34","car_id":"1","car_name":"รถ Golf คันที่ 1","car_type":"รถ Golf","routeline_id":"1","routeline_type":"สายสีเขียว"}
        $scope.takeStep1 = function(){
            $scope.action_step = 1;
            console.log("booking called");

            var serviceURL = "getRoutes";
            var serviceParams = [{value:999}];
            var response = getBookingService.getProperty(serviceURL, serviceParams);
            response.then(function(value){
                $scope.routes = value;
                console.log("data value ="+value);

            }, function(error){
                console.log("service error = "+error);
                $scope.msgShow = true;
            });

        /*    var watch;
            watch = $timeout(function(){
//                $scope.cars = [];
                setDetails();
            },5000);
            */
        }

        $scope.takeStep2 = function(id){
            $scope.action_step = 2;
            var serviceURL = "getRunningTime";
            var serviceParams = [{value:999}];
            var response = getBookingService.getProperty(serviceURL, serviceParams);
            response.then(function(value){
                $scope.runningTime = value;
                console.log("data value2 ="+value.size);

            }, function(error){
                console.log("service error = "+error);
                $scope.msgShow = true;
            });
        };

        $scope.takeStep3 = function(running_time_id, route_id, time, destination){
            if (confirm('Would you like to confirm?')) {
                $rootScope.time = time;
                $rootScope.destination = destination;
                $scope.takeStep4(running_time_id, route_id);
            }
        };

        $scope.takeStep4 = function(running_time_id, route_id){
            $scope.action_step = 4;
            var serviceURL = "confirmBooking";
            var serviceParams = [{route_id:route_id},{running_time_id:running_time_id}];
            var response = getBookingService.getProperty(serviceURL, serviceParams);
            response.then(function(value){

                $scope.bookingData = value;
//                $scope.ticket = 'confirmed';
                console.log("data value ="+value);

            }, function(error){
                console.log("service error = "+error);
                $scope.msgShow = true;
            });
        };

        $scope.takeStep2(); // run first step
    });



